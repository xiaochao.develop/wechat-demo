// pages/add-note/add-note.js
const util = require('../../utils/util.js')
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    normalTime: null,
    title: null,
    content: null,
    completeTime: null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(Date.now())
    this.setData({
      completeTime: util.formatTime(new Date(Date.now())),
      normalTime: util.formatSimpleTime(new Date(Date.now()))
    })
  },
  onSaveNote: function(event) { //保存
    var that = this;
    if (that.data.title == null) {
      wx.showToast({
        title: '请输入标题',
      })
    } else if (that.data.content == null) {
      wx.showToast({
        title: '请输入内容',
      })
    } else { //保存到本地
      app.globalData.noteData.normalTime = that.data.normalTime,
        app.globalData.noteData.completeTime = that.data.completeTime,
        app.globalData.noteData.title = that.data.title,
        app.globalData.noteData.content = that.data.content

      //保存一条笔记到本地
      var notes = wx.getStorageSync('notes') || []
      notes.unshift(app.globalData.noteData)
      wx.setStorageSync('notes', notes)
      wx.navigateBack({

      })
    }

  },
  watchTitleInput: function(event) { //绑定title输入框
    console.log("title", event.detail.value)
    this.setData({
      title: event.detail.value
    })
  },
  watchContentInput: function(event) { //绑定content输入框
    console.log("content", event.detail.value)
    this.setData({
      content: event.detail.value
    })
  }

})