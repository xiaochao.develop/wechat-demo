// pages/main/main.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    headUrl: null,
    notes: []
  },

  onShow: function() {
    var that = this
    wx.getStorage({
      key: 'notes',
      success: function(res) {
        console.log("notes", res)
        that.setData({
          notes: res.data
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this
    that.setData({ //设置头像
      headUrl: options.headUrl
    })

  },
  bindTouchStart: function(e) {
    this.startTime = e.timeStamp;
  },
  bindTouchEnd: function(e) {
    this.endTime = e.timeStamp;
  },
  onAddNote: function() { //新建笔记
    wx.navigateTo({
      url: '/pages/add-note/add-note',
    })
  },
  onDetail: function(event) { //跳转到详情
    console.log("点击了笔记", event.target.dataset.index + "")
    if (this.endTime - this.startTime < 350) {
      wx.navigateTo({
        url: '/pages/detail/detail?index=' + event.target.dataset.index,
      })
    }
  },
  onLongClick: function(event) { //长按删除
    var that = this
    wx.showModal({
      title: '删除该条笔记',
      content: '您确定要删除该条笔记吗？删除后不可找回',
      cancelText: "取消",
      confirmText: "确定",
      success: function(res) {
        if (res.confirm) {
          that: wx.getStorage({
            key: 'notes',
            success: function(res) {
              //删除数组中的index数据，第二个参数为要删除的个数
              console.log("index=", event.target.dataset.index)
              console.log("storage=", res.data)
              var notess = res.data
              notess.splice(event.target.dataset.index, 1)
              that: wx.setStorage({
                key: 'notes',
                data: notess,
                success: {
                  //刷新
                  that: wx.showToast({
                    title: '已删除',
                  }),
                  that: that.setData({
                    notes: res.data
                  })
                }
              })
            },
          })
        }
      }
    })
  }
})