// pages/welcome/welcome.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    userInfo: {},
    hasUserInfo: false,
  },
  onLoad: function() {
    if (app.globalData.userInfo) { //app全局有数据
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse) { //有开放能力
      console.log("userInfo", "this.data.canIUse==true"),
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => { 
        console.log("userInfo", res.userInfo),
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else { //没有开放能力，做兼容处理
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
  getUserInfo: function(e) {
    console.log("hello",e.detail.userInfo)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true,
    })
    if(e.detail.userInfo!=undefined){//同意权限
      wx.redirectTo({
        url: '/pages/main/main?headUrl='+e.detail.userInfo.avatarUrl,
      })
    }else{
      wx.showToast({
        title: '未获取到权限',
      })
    }
  },onStartMain:function(){
    console.log("userInfo", app.globalData.userInfo)
    if (app.globalData.userInfo){//已经获取到数据
      wx.redirectTo({
        url: '/pages/main/main?headUrl=' + app.globalData.userInfo.avatarUrl,
      })
    }
  }
})