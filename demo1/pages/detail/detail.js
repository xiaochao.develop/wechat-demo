// pages/detail/detail.js

Page({

  /**
   * 页面的初始数据
   */
  data: {
    noteData: {
      title: null,
      content: null,
      completeTime: null
    },
    index:-1,
    isEnable:true//是否禁用input
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("index",options.index)
    var that = this
    var index = options.index
    that.setData({
      index:index
    })
    wx.getStorage({
      key: 'notes',
      success: function(res) {
        console.log("缓存数据", res.data[index])
        that.setData({
          noteData:res.data[index]
        })
      },
    })
  },
  onDelete:function(){
    var that = this
    wx.showModal({
      title: '删除笔记',
      content: '您确定要删除该条笔记吗？删除后不可找回',
      cancelText:"取消",
      confirmText:"确定",
      success:function(res){
        if (res.confirm) {
          that: wx.getStorage({
            key: 'notes',
            success: function (res) {
              //删除数组中的index数据，第二个参数为要删除的个数
              console.log("index=",that.data.index)
              console.log("storage=", res.data)
              var notes = res.data
              notes.splice(that.data.index,1)
              that:wx.setStorage({
                key: 'notes',
                data: notes,
                success:{
                  that:wx.navigateBack({
                  
                  })
                }
              })
            },
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      },
    })
  },
  onEdit:function(){
    this.setData({
      isEnable:false
    })
  }
})