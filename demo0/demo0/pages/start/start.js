// pages/start/start.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [{
        name: "小陈",
        age: 20
      },
      {
        name: "小宇",
        age: 30
      },
      {
        name: "小明",
        age: 40
      }
    ]
  },

  onStartWork: function() {
    wx.navigateTo({
      url: '/pages/work/work',
    })
  }

})